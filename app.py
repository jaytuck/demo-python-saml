from flask import Flask, url_for, session, Response
from pprint import pformat
import flask_saml

app = Flask(__name__)

app.config.update({
    'SECRET_KEY': 'dev',
    'SAML_METADATA_URL': 'https://login.microsoftonline.com/9f248767-8e1a-42f3-836f-c092ab95ff70/federationmetadata/2007-06/federationmetadata.xml?appid=02f2a831-0ac0-45ea-b97b-b348bf800fba',
})

flask_saml.FlaskSAML(app)
def process_login(sender, subject, attributes, auth):
    print(f'saving {subject} and {attributes}')
    session['saml'] = {
            'subject': subject,
            'attributes': attributes,
    }
flask_saml.saml_authenticated.connect(process_login, app)


@app.route('/')
def index():
    return f'<a href="{ url_for("login", next="http://127.0.0.1:5000/showlogindetails") }">Login</a><br><a href="/logout">Log Out</a>'

@app.route('/logout')
def logout_session():
    print(f'removing session data: {session.pop("saml")}')
    return 'logged out<br><a href="/">Home</a>'

@app.route('/showlogindetails')
def showlogindetails():
    #breakpoint()
    return Response(
        pformat(session['saml']),
        mimetype='text/plain'
    )


